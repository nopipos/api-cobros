<?php

//include("OrdenPagoWS.php");

class STPSign {

    private $cadenaOriginal = "";
    private $privatekey = "";
    private $passphrase = "";

    public function __construct($ordenPagoWs, $privatekey, $passphrase) {
        $this->privatekey = $privatekey;
        $this->passphrase = $passphrase;
        $this->cadenaOriginal = "||" .
                $ordenPagoWs->getInstitucionContraparte() . "|" . //a
                $ordenPagoWs->getEmpresa() . "|" . //b
                $ordenPagoWs->getFechaOperacion() . "|" . //c
                $ordenPagoWs->getFolioOrigen() . "|" . //d
                $ordenPagoWs->getClaveRastreo() . "|" . //e
                $ordenPagoWs->getInstitucionOperante() . "|" . //f
                number_format($ordenPagoWs->getMonto(),2) . "|" . //g
                $ordenPagoWs->getTipoPago() . "|" . //h
                $ordenPagoWs->getTipoCuentaOrdenante() . "|" . //i
                $ordenPagoWs->getNombreOrdenante() . "|" . //j
                $ordenPagoWs->getCuentaOrdenante() . "|" . //k
                $ordenPagoWs->getRfcCurpOrdenante() . "|" . //l
                $ordenPagoWs->getTipoCuentaBeneficiario() . "|" . //m
                $ordenPagoWs->getNombreBeneficiario() . "|" . //n
                $ordenPagoWs->getCuentaBeneficiario() . "|" . //o
                $ordenPagoWs->getRfcCurpBeneficiario() . "|" . //p
                $ordenPagoWs->getEmailBeneficiario() . "|" . //q
                $ordenPagoWs->getTipoCuentaBeneficiario2() . "|" . //r
                $ordenPagoWs->getNombreBeneficiario2() . "|" . //s
                $ordenPagoWs->getCuentaBeneficiario2() . "|" . //t
                $ordenPagoWs->getRfcCurpBeneficiario2() . "|" . //u
                $ordenPagoWs->getConceptoPago() . "|" . //v
                $ordenPagoWs->getConceptoPago2() . "|" . //w
                $ordenPagoWs->getClaveCatUsuario1() . "|" . //x
                $ordenPagoWs->getClaveCatUsuario2() . "|" . //y
                $ordenPagoWs->getClavePago() . "|" . //z
                $ordenPagoWs->getReferenciaCobranza() . "|" . //aa
                $ordenPagoWs->getReferenciaNumerica() . "|" . //bb
                $ordenPagoWs->getTipoOperacion() . "|" . //cc
                $ordenPagoWs->getTopologia() . "|" . //dd
                $ordenPagoWs->getUsuario() . "|" . //ee
                $ordenPagoWs->getMedioEntrega() . "|" . //ff
                $ordenPagoWs->getPrioridad() . "|" . //gg
                number_format($ordenPagoWs->getIva(),2)."||"; //hh
        echo "Cadena original: ".$this->cadenaOriginal."\n";
    }

    public function getSign() {
        $privateKey = $this->getCertified();
        $binarySign="";
        openssl_sign($this->cadenaOriginal, $binarySign, $privateKey, "RSA-SHA256");
        $sign = base64_encode($binarySign);
        openssl_free_key($privateKey);
        return $sign;
    }

    private function getCertified() {
        $fp = fopen($this->privatekey, "r");
        $privateKey = fread($fp, filesize($this->privatekey));
        fclose($fp);
        return openssl_get_privatekey($privateKey, $this->passphrase);
    }

}
