<?php


namespace App\Service\email;


use App\Entity\Empresas;

class Email
{
    /**
     *
     * @param String  $de base64_encode desde donde se manda el correo base64
     * @param String  $de_nombre base64_encode el nombre de quien manda el correo
     * @param String[] $para para quien va dirigido el correo
     * @param String $asunto  base64_encodeel asunto del correo base64
     * @param String[] $adjuntos los adjuntos de los archivos name y body
     * @param String[]  $copia base64_encode si es que se tiene que mandar una copia
     * @param String  $mensaje base64_encode el cuerpo del mensaje a enviar base64
     * @param Empresas  $empresa Entity Empresa
     * @return boolean
     */
    public static function enviarCorreo($de_nombre, $para, $asunto, $adjuntos, $copia, $mensaje,$empresa) {

        $hostUrl = "http://www.clickfactura.com.mx/rest/enviacorreo/mail/enviar/";

        $host = $empresa->getHost();
        $usuario = $empresa->getMail();
        $usuario = str_replace("@mailtrap.io","",$usuario);
        $password =$empresa->getPassword();
        $port = $empresa->getPuerto();
        $config = array(
            'host' => $host,
            'port' => $port,
            'usuario' => $usuario,
            //'usuario' => "recepcion@clickfactura.com.mx",
            'password' => base64_encode($password),
            //'password' => base64_encode("Q(XB(1-?*0cD"),
        );

        $usuario = $empresa->getMail();
        $datos = array(
            'body' => $mensaje,
            'asunto' => $asunto,
            'from' => base64_encode($usuario),
            //'from' => base64_encode("recepcion@clickfactura.com.mx"),
            'from_name' => $de_nombre,
            'cc' => $copia,
            'to' => $para,
            'files' => $adjuntos,
            'config' => $config,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $hostUrl);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datos));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            )
        );
        $result = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($result, true);

        /*if($asunto == "UmVjb3JkYXRvcmlvIGRlIHBhZ28gKGZhY3R1cmEgdmVuY2lkYSk="){
            echo "re";
            var_dump($result);
            exit;
        }*/

        if($result['error_code'] == "200")
            return true;
        else
            return false;

    }
}