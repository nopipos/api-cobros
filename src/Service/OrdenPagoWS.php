<?php


namespace App\Service;


class OrdenPagoWS
{
    private $causaDevolucion=1;
    private $claveCatUsuario1="";
    private $claveCatUsuario2="";
    private $clavePago="";
    private $claveRastreo="";
    private $claveRastreoDevolucion="";
    private $conceptoPago="";
    private $conceptoPago2="";
    private $cuentaBeneficiario="";
    private $cuentaBeneficiario2="";
    private $cuentaOrdenante="";
    private $emailBeneficiario="";
    private $empresa="";
    private $estado="";
    private $facturas;
    private $fechaOperacion;
    private $firma="";
    private $folioOrigen="";
    private $idCliente="";
    private $idEF="";
    private $institucionContraparte="";
    private $institucionOperante="";
    private $iva=0.00;
    private $medioEntrega="3";
    private $monto="";
    private $montoInteres=0.0;
    private $montoOriginal=0.0;
    private $nombreBeneficiario="";
    private $nombreBeneficiario2="";
    private $nombreOrdenante="";
    private $prioridad="";
    private $referenciaCobranza="";
    private $referenciaNumerica="";
    private $reintentos="";
    private $rfcCurpBeneficiario="";
    private $rfcCurpBeneficiario2="";
    private $rfcCurpOrdenante="";
    private $tipoCuentaBeneficiario="";
    private $tipoCuentaBeneficiario2="";
    private $tipoCuentaOrdenante="";
    private $tipoOperacion="";
    private $tipoPago="";
    private $topologia="T";
    private $tsAcuseBanxico="";
    private $tsAcuseConfirmacion="";
    private $tsCaptura="";
    private $tsConfirmacion="";
    private $tsDevolucion="";
    private $tsDevolucionRecibida="";
    private $tsEntrega="";
    private $tsLiquidacion="";
    private $usuario="";

    public function __construct() {
    }
    function getCausaDevolucion() {
        return $this->causaDevolucion;
    }

    function getClaveCatUsuario1() {
        return $this->claveCatUsuario1;
    }

    function getClaveCatUsuario2() {
        return $this->claveCatUsuario2;
    }

    function getClavePago() {
        return $this->clavePago;
    }

    function getClaveRastreo() {
        return $this->claveRastreo;
    }

    function getClaveRastreoDevolucion() {
        return $this->claveRastreoDevolucion;
    }

    function getConceptoPago() {
        return $this->conceptoPago;
    }

    function getConceptoPago2() {
        return $this->conceptoPago2;
    }

    function getCuentaBeneficiario() {
        return $this->cuentaBeneficiario;
    }

    function getCuentaBeneficiario2() {
        return $this->cuentaBeneficiario2;
    }

    function getCuentaOrdenante() {
        return $this->cuentaOrdenante;
    }

    function getEmailBeneficiario() {
        return $this->emailBeneficiario;
    }

    function getEmpresa() {
        return $this->empresa;
    }

    function getEstado() {
        return $this->estado;
    }

    function getFechaOperacion() {
        return $this->fechaOperacion;
    }

    function getFirma() {
        return $this->firma;
    }

    function getFolioOrigen() {
        return $this->folioOrigen;
    }

    function getIdCliente() {
        return $this->idCliente;
    }

    function getIdEF() {
        return $this->idEF;
    }

    function getInstitucionContraparte() {
        return $this->institucionContraparte;
    }

    function getInstitucionOperante() {
        return $this->institucionOperante;
    }

    function getIva() {
        return $this->iva;
    }

    function getMedioEntrega() {
        return $this->medioEntrega;
    }

    function getMonto() {
        return $this->monto;
    }

    function getMontoInteres() {
        return $this->montoInteres;
    }

    function getMontoOriginal() {
        return $this->montoOriginal;
    }

    function getNombreBeneficiario() {
        return $this->nombreBeneficiario;
    }

    function getNombreBeneficiario2() {
        return $this->nombreBeneficiario2;
    }

    function getNombreOrdenante() {
        return $this->nombreOrdenante;
    }

    function getPrioridad() {
        return $this->prioridad;
    }

    function getReferenciaCobranza() {
        return $this->referenciaCobranza;
    }

    function getReferenciaNumerica() {
        return $this->referenciaNumerica;
    }

    function getReintentos() {
        return $this->reintentos;
    }

    function getRfcCurpBeneficiario() {
        return $this->rfcCurpBeneficiario;
    }

    function getRfcCurpBeneficiario2() {
        return $this->rfcCurpBeneficiario2;
    }

    function getRfcCurpOrdenante() {
        return $this->rfcCurpOrdenante;
    }

    function getTipoCuentaBeneficiario() {
        return $this->tipoCuentaBeneficiario;
    }

    function getTipoCuentaBeneficiario2() {
        return $this->tipoCuentaBeneficiario2;
    }

    function getTipoCuentaOrdenante() {
        return $this->tipoCuentaOrdenante;
    }

    function getTipoOperacion() {
        return $this->tipoOperacion;
    }

    function getTipoPago() {
        return $this->tipoPago;
    }

    function getTopologia() {
        return $this->topologia;
    }

    function getTsAcuseBanxico() {
        return $this->tsAcuseBanxico;
    }

    function getTsAcuseConfirmacion() {
        return $this->tsAcuseConfirmacion;
    }

    function getTsCaptura() {
        return $this->tsCaptura;
    }

    function getTsConfirmacion() {
        return $this->tsConfirmacion;
    }

    function getTsDevolucion() {
        return $this->tsDevolucion;
    }

    function getTsDevolucionRecibida() {
        return $this->tsDevolucionRecibida;
    }

    function getTsEntrega() {
        return $this->tsEntrega;
    }

    function getTsLiquidacion() {
        return $this->tsLiquidacion;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function setCausaDevolucion($causaDevolucion) {
        $this->causaDevolucion = $causaDevolucion;
    }

    function setClaveCatUsuario1($claveCatUsuario1) {
        $this->claveCatUsuario1 = $claveCatUsuario1;
    }

    function setClaveCatUsuario2($claveCatUsuario2) {
        $this->claveCatUsuario2 = $claveCatUsuario2;
    }

    function setClavePago($clavePago) {
        $this->clavePago = $clavePago;
    }

    function setClaveRastreo($claveRastreo) {
        $this->claveRastreo = $claveRastreo;
    }

    function setClaveRastreoDevolucion($claveRastreoDevolucion) {
        $this->claveRastreoDevolucion = $claveRastreoDevolucion;
    }

    function setConceptoPago($conceptoPago) {
        $this->conceptoPago = $conceptoPago;
    }

    function setConceptoPago2($conceptoPago2) {
        $this->conceptoPago2 = $conceptoPago2;
    }

    function setCuentaBeneficiario($cuentaBeneficiario) {
        $this->cuentaBeneficiario = $cuentaBeneficiario;
    }

    function setCuentaBeneficiario2($cuentaBeneficiario2) {
        $this->cuentaBeneficiario2 = $cuentaBeneficiario2;
    }

    function setCuentaOrdenante($cuentaOrdenante) {
        $this->cuentaOrdenante = $cuentaOrdenante;
    }

    function setEmailBeneficiario($emailBeneficiario) {
        $this->emailBeneficiario = $emailBeneficiario;
    }

    function setEmpresa($empresa) {
        $this->empresa = $empresa;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function setFechaOperacion($fechaOperacion) {
        $this->fechaOperacion = $fechaOperacion;
    }

    function setFirma($firma) {
        $this->firma = $firma;
    }

    function setFolioOrigen($folioOrigen) {
        $this->folioOrigen = $folioOrigen;
    }

    function setIdCliente($idCliente) {
        $this->idCliente = $idCliente;
    }

    function setIdEF($idEF) {
        $this->idEF = $idEF;
    }

    function setInstitucionContraparte($institucionContraparte) {
        $this->institucionContraparte = $institucionContraparte;
    }

    function setInstitucionOperante($institucionOperante) {
        $this->institucionOperante = $institucionOperante;
    }

    function setIva($iva) {
        $this->iva = $iva;
    }

    function setMedioEntrega($medioEntrega) {
        $this->medioEntrega = $medioEntrega;
    }

    function setMonto($monto) {
        $this->monto = $monto;
    }

    function setMontoInteres($montoInteres) {
        $this->montoInteres = $montoInteres;
    }

    function setMontoOriginal($montoOriginal) {
        $this->montoOriginal = $montoOriginal;
    }

    function setNombreBeneficiario($nombreBeneficiario) {
        $this->nombreBeneficiario = $nombreBeneficiario;
    }

    function setNombreBeneficiario2($nombreBeneficiario2) {
        $this->nombreBeneficiario2 = $nombreBeneficiario2;
    }

    function setNombreOrdenante($nombreOrdenante) {
        $this->nombreOrdenante = $nombreOrdenante;
    }

    function setPrioridad($prioridad) {
        $this->prioridad = $prioridad;
    }

    function setReferenciaCobranza($referenciaCobranza) {
        $this->referenciaCobranza = $referenciaCobranza;
    }

    function setReferenciaNumerica($referenciaNumerica) {
        $this->referenciaNumerica = $referenciaNumerica;
    }

    function setReintentos($reintentos) {
        $this->reintentos = $reintentos;
    }

    function setRfcCurpBeneficiario($rfcCurpBeneficiario) {
        $this->rfcCurpBeneficiario = $rfcCurpBeneficiario;
    }

    function setRfcCurpBeneficiario2($rfcCurpBeneficiario2) {
        $this->rfcCurpBeneficiario2 = $rfcCurpBeneficiario2;
    }

    function setRfcCurpOrdenante($rfcCurpOrdenante) {
        $this->rfcCurpOrdenante = $rfcCurpOrdenante;
    }

    function setTipoCuentaBeneficiario($tipoCuentaBeneficiario) {
        $this->tipoCuentaBeneficiario = $tipoCuentaBeneficiario;
    }

    function setTipoCuentaBeneficiario2($tipoCuentaBeneficiario2) {
        $this->tipoCuentaBeneficiario2 = $tipoCuentaBeneficiario2;
    }

    function setTipoCuentaOrdenante($tipoCuentaOrdenante) {
        $this->tipoCuentaOrdenante = $tipoCuentaOrdenante;
    }

    function setTipoOperacion($tipoOperacion) {
        $this->tipoOperacion = $tipoOperacion;
    }

    function setTipoPago($tipoPago) {
        $this->tipoPago = $tipoPago;
    }

    function setTopologia($topologia) {
        $this->topologia = $topologia;
    }

    function setTsAcuseBanxico($tsAcuseBanxico) {
        $this->tsAcuseBanxico = $tsAcuseBanxico;
    }

    function setTsAcuseConfirmacion($tsAcuseConfirmacion) {
        $this->tsAcuseConfirmacion = $tsAcuseConfirmacion;
    }

    function setTsCaptura($tsCaptura) {
        $this->tsCaptura = $tsCaptura;
    }

    function setTsConfirmacion($tsConfirmacion) {
        $this->tsConfirmacion = $tsConfirmacion;
    }

    function setTsDevolucion($tsDevolucion) {
        $this->tsDevolucion = $tsDevolucion;
    }

    function setTsDevolucionRecibida($tsDevolucionRecibida) {
        $this->tsDevolucionRecibida = $tsDevolucionRecibida;
    }

    function setTsEntrega($tsEntrega) {
        $this->tsEntrega = $tsEntrega;
    }

    function setTsLiquidacion($tsLiquidacion) {
        $this->tsLiquidacion = $tsLiquidacion;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function getFacturas() {
        return $this->facturas;
    }

    function setFacturas($facturas) {
        $this->facturas = $facturas;
    }

}