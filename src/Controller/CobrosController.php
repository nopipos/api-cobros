<?php

namespace App\Controller;

use App\Entity\Clientes;
use App\Entity\Empresas;
use App\Entity\Pagos;
use App\Entity\Plazas;
use App\Entity\Sistemas;
use App\Service\OrdenPagoWS;
use App\Service\STPSign;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;

class CobrosController extends AbstractController
{

    /**
     * @Route("/", name="index", methods={"GET"})
     */

    public function index1(Request $request): Response{

        return $this->json([
            'env' => "api-test",
        ],200);
    }

    /**
     * @Route("/cobro/generar", name="generar", methods={"POST"})
     */

    public function index(Request $request): Response{

        $datos = json_decode($request->getContent(),true);

        error_reporting(E_ERROR | E_WARNING | E_PARSE);
        require_once ('./../nusoap/nusoap.php');

        $cliente = new \nusoap_client("https://demo.stpmex.com:7024/spei/webservices/SpeiActualizaServices?wsdl", false, "", "", "", "");

        $mynameSpace = "http://h2h.integration.spei.enlacefi.lgec.com/";
        $ordenPagoWs = new OrdenPagoWS();
        $ordenPagoWs->setEmpresa("CLICK_FACTURA");
        $ordenPagoWs->setMonto(200.00);
        $ordenPagoWs->setReferenciaNumerica(235542);
        $ordenPagoWs->setConceptoPago("Concepto pago test 4");
        $ordenPagoWs->setInstitucionOperante("90646");
        $ordenPagoWs->setInstitucionContraparte("846");
        $ordenPagoWs->setCuentaBeneficiario("8461800004000001");
        $ordenPagoWs->setTipoCuentaBeneficiario(3);
        $ordenPagoWs->setClaveRastreo("4500571996381");
        $ordenPagoWs->setNombreBeneficiario("juan");
        $ordenPagoWs->setNombreOrdenante("click");
        $ordenPagoWs->setTipoPago(1);
        $ordenPagoWs->setTipoOperacion(1);
        $ordenPagoWs->setTopologia("T");
        //$ordenPagoWs->setUsuario("CLICK_FACTURA");
        $ordenPagoWs->setFechaOperacion("20210806");
        $ordenPagoWs->setFolioOrigen("4500000");
        $ordenPagoWs->setCuentaOrdenante("646180270200000008");
        $ordenPagoWs->setRfcCurpBeneficiario("TEAJ8705195");
        $ordenPagoWs->setTipoCuentaOrdenante(40);
        $ordenPagoWs->setIva(0.00);
        $ordenPagoWs->setMontoInteres(0);
        $ordenPagoWs->setMontoOriginal(0);
        $ordenPagoWs->setMedioEntrega(3);

        //$firmaObj = new STPSign($ordenPagoWs,"./../access/click_factura_sa_de_cv_cfa_.pem","C/%(J#S87*Tadf2=3");
        $firmaObj = new STPSign($ordenPagoWs,"./../access/prueba-key.pem","12345678");

        $ordenPagoWs->setFirma($firmaObj->getSign());


        $result = $cliente->call(
            'registraOrden', array(
                'ordenPago'=>array(
                    /*'causaDevolucion' => null,
                    'claveCatUsuario1' => null,
                    'claveCatUsuario2' => null,*/
                    'claveRastreo' => $ordenPagoWs->getClaveRastreo(),
                    //'claveRastreoDevolucion' => null,
                    'conceptoPago' => $ordenPagoWs->getConceptoPago(),
                    //'conceptoPago2' => null,
                    'cuentaBeneficiario' => $ordenPagoWs->getCuentaBeneficiario(),
                    //'cuentaBeneficiario2' => null,
                    'cuentaOrdenante' => $ordenPagoWs->getCuentaOrdenante(),
                    /*'digitoIdentificadorBeneficiario' => null,
                    'digitoIdentificadorOrdenante' => null,
                    'emailBeneficiario' => null,*/
                    'empresa' => $ordenPagoWs->getEmpresa(),
                    /*'error' => null,
                    'estado' => null,
                    'facturas' => null,
                    'fechaLimitePago' => null,*/
                    'fechaOperacion' => $ordenPagoWs->getFechaOperacion(),
                    'firma' => $ordenPagoWs->getFirma(),
                    'folioOrigen' => $ordenPagoWs->getFolioOrigen(),
                    /*'folioPlataforma' => null,
                    'horaServidorBanxico' => null,*/
                    'idCliente' => null,
                    'idEF' => null,
                    //'instancia' => null,
                    'institucionContraparte' => $ordenPagoWs->getInstitucionContraparte(),
                    'institucionOperante' => $ordenPagoWs->getInstitucionOperante(),
                    'iva' =>number_format($ordenPagoWs->getIva(),2),
                    'medioEntrega' => $ordenPagoWs->getMedioEntrega(),
                    'monto' => number_format($ordenPagoWs->getMonto(),2),
                    //'montoComision' => null,
                    'montoInteres' => $ordenPagoWs->getMontoInteres(),
                    'montoOriginal' => $ordenPagoWs->getMontoOriginal(),
                    'nombreBeneficiario' => $ordenPagoWs->getNombreBeneficiario(),
                    /*'nombreBeneficiario2' => null,
                    'nombreCEP' => null,*/
                    'nombreOrdenante' => $ordenPagoWs->getNombreOrdenante(),
                    /*'numCelularBeneficiario' => null,
                    'numCelularOrdenante' => null,
                    'pagoComision' => null,
                    'prioridad' => null,
                    'referenciaCobranza' => null,*/
                    'referenciaNumerica' => $ordenPagoWs->getReferenciaNumerica(),
                    /*'reintentos' => null,
                    'rfcCEP' => null,*/
                    'rfcCurpBeneficiario' => $ordenPagoWs->getRfcCurpBeneficiario(),
                    /*'rfcCurpBeneficiario2' => null,
                    'rfcCurpOrdenante' => null,
                    'sello' => null,
                    'serieCertificado' => null,
                    'swift1' => null,
                    'swift2' => null,*/
                    'tipoCuentaBeneficiario' => $ordenPagoWs->getTipoCuentaBeneficiario(),
                    /*'tipoCuentaBeneficiario2' => null,*/
                    'tipoCuentaOrdenante' => $ordenPagoWs->getTipoCuentaOrdenante(),
                    'tipoOperacion' => $ordenPagoWs->getTipoOperacion(),
                    'tipoPago' => $ordenPagoWs->getTipoPago(),
                    'topologia' => $ordenPagoWs->getTopologia(),
                    /*'tsAcuseBanxico' => null,
                    'tsAcuseConfirmacion' => null,
                    'tsCaptura' => null,
                    'tsConfirmacion' => null,
                    'tsDevolucion' => null,
                    'tsDevolucionRecibida' => null,
                    'tsEntrega' => null,
                    'tsLiquidacion' => null,
                    'uetr' => null,*/
                    'urlCEP' => "https://pay.clickfactura.app/app/cobro/recibirqa",
                    'usuario' => $ordenPagoWs->getUsuario(),
                ),
        ),$mynameSpace);

        echo '<h2>Request</h2>';
        echo '<pre>' . htmlspecialchars($cliente->request, ENT_QUOTES) . '</pre>';
        echo '<h2>Response</h2>';
        echo '<pre>' . htmlspecialchars($cliente->response, ENT_QUOTES) . '</pre>';
        exit;

        #comentario
        if ($cliente->fault) {
            echo 'Error: ';
            print_r($result);
        } else {
            // check result
            $err_msg = $cliente->getError();
            if ($err_msg) {
                // Print error msg
                echo 'Error: '.$err_msg;
            } else {
                // Print result
                echo 'Result: ';
                print_r($result);
            }
        }

        print_r($result);
        exit;

        return $this->json([],200);

    }

    /**
     * @Route("/cobro/recibir", name="recibir", methods={"POST"})
     */

    public function recibirQa(Request $request):Response{

        $datos = json_decode($request->getContent(),true);
        if(!is_dir("./../respuestas/"))
            mkdir("./../respuestas/");
        $date = date("Y-m-d H:i:s",strtotime($datos['fechaOperacion']));

        $nombre = date("YmdHis");

        #busco el pago qu correponde a la informacion que me envian

        $pagosRepo = $this->getDoctrine()->getRepository(Pagos::class);
        $pago = $pagosRepo->findOneBy(['referencia'=>$datos['referenciaNumerica'],'cuenta_beneficiario'=>$datos['cuentaBeneficiario'],'monto'=>$datos['monto'],'estatus'=>1]);
        $em=$this->getDoctrine()->getManager();

        if($pago == null){

            #busco el cliente por la cuenta para saber si es de un cliente que trabaja sin referencias

            $clienteRepo = $this->getDoctrine()->getRepository(Clientes::class);
            $cliente = $clienteRepo->findOneBy(['clabe'=>trim($datos['cuentaBeneficiario'])]);

            if($cliente != null){
                $cliente->getEmpresa();

                $empresaRepo = $this->getDoctrine()->getRepository(Empresas::class);
                $mpresa = $empresaRepo->findOneBy(['id'=>$cliente->getEmpresa()->getId()]);

                #saco el primer sistema
                $sistemasRepo = $this->getDoctrine()->getRepository(Sistemas::class);
                $sistema = $sistemasRepo->findOneBy(['empresa'=>$cliente->getEmpresa()->getId(),'estatus'=>1]);


            }
            else{
                $empresaRepo = $this->getDoctrine()->getRepository(Empresas::class);
                $mpresa = $empresaRepo->findOneBy(['id'=>1]);

                $clienteRepo = $this->getDoctrine()->getRepository(Clientes::class);
                $cliente = $clienteRepo->findOneBy(['rfc'=>'XAXX010101000','empresa'=>$mpresa->getId()]);

            }

            #si no se encontro el pago entonces lo registro para posterior a eso asignalo manualmente
            $pagoQueNExiste = new Pagos();
            if($sistema != null){
                $pagoQueNExiste->setSistema($sistema);
                $pagoQueNExiste->setEstatus(1);
                $pagoQueNExiste->setComentario("Pago sin referencia");


            }else{
                $pagoQueNExiste->setEstatus(4);
                $pagoQueNExiste->setComentario("No se encontro el pago a relacionar");
            }

            $pagoQueNExiste->setEmpresa($mpresa);
            $pagoQueNExiste->setCliente($cliente);
            $pagoQueNExiste->setClaveRastreo($datos['claveRastreo']);
            $pagoQueNExiste->setCuentaOrdenante($datos['cuentaOrdenante']);
            $pagoQueNExiste->setConcepto($datos['conceptoPago']);
            $pagoQueNExiste->setFechaOperacionBanco(\DateTime::createFromFormat('Y-m-d H:i:s',$date));
            $pagoQueNExiste->setFechaOperacion(\DateTime::createFromFormat('Y-m-d H:i:s',date("Y-m-d H:i:s")));
            $pagoQueNExiste->setNumeroOperacion($datos['id']);
            $pagoQueNExiste->setCuentaBeneficiario($datos['cuentaBeneficiario']);
            $pagoQueNExiste->setMoneda("MXN");
            $pagoQueNExiste->setReferencia($datos['referenciaNumerica']);
            $pagoQueNExiste->setMonto($datos['monto']);

            $em->persist($pagoQueNExiste);
            $em->flush();

            #mando el mail a los administradores para que revisen este pago

            if($sistema != null){

                $host = $sistema->getApi();

                if(false){
                    $pago->setComentario("ERROR API INTERNA NO EXISTE  ".$host);
                    $em->persist($pago);
                    $em->flush();

                    $body = $this->renderView('cobros/error_api.html.twig',[
                        'error'=>"ERROR API INTERNA NO EXISTE  ".$host
                    ]);

                    \App\Service\email\Email::enviarCorreo(base64_encode($pago->getEmpresa()->getMailDesde()),[$pago->getSistema()->getDesarrollador()],base64_encode("Error al consumir la api de confirmar el pago"),null,base64_encode("juan.sotero@clickfactura.mx"),base64_encode($body),$pago->getEmpresa());

                }
                else{
                    try{
                        $client = HttpClient::create();
                        $response = $client->request('POST', $host,
                            [
                                'headers' => [
                                    'Content-Type' => 'application/json',
                                    'token' => $sistema->getToken(),
                                ],
                                //'body' => ['parameter1' => 'value1', '...'],
                                'json' => [
                                    'referencia' => $pagoQueNExiste->getReferencia(),
                                    'estatus'=> "Pago exitoso",
                                    'clabe'=> $pagoQueNExiste->getCuentaBeneficiario(),
                                    'monto'=> $pagoQueNExiste->getMonto(),
                                    'exito'=>true,
                                ],
                            ]
                        );
                    }catch (\Exception $ex){

                    }
                    $statusCode = $response->getStatusCode();

                    if($statusCode != 200){

                        $pago->setComentario("ERROR API INTERNA: ".print_r($response,true));

                        $em->persist($pago);
                        $em->flush();
                    }
                }

            }
            else{
                $body = $this->renderView('cobros/pago_no_reconocido.html.twig',[
                    'cuenta_ordenante'=>$pagoQueNExiste->getCuentaOrdenante(),
                    'clave_rastero'=>$pagoQueNExiste->getClaveRastero(),
                    'concepto'=>$pagoQueNExiste->getConcepto(),
                    'fecha_operacion_banco'=>$pagoQueNExiste->getFechaOperacionBanco(),
                    'fecha_operacion'=>$pagoQueNExiste->getFechaOperacion(),
                    'numero_operacion'=>$pagoQueNExiste->getNumeroOperacion(),
                    'cuenta_beneficiario'=>$pagoQueNExiste->getCuentaBeneficiario(),
                    'moneda'=>$pagoQueNExiste->getMoneda(),
                    'referencia'=>$pagoQueNExiste->getReferencia(),
                    'monto'=>$pagoQueNExiste->getMonto(),
                ]);

                \App\Service\email\Email::enviarCorreo(base64_encode($pagoQueNExiste->getEmpresa()->getMailDesde()),['juan.sotero@clickfactura.mx'],base64_encode("Pago recibido no reconocido"),null,base64_encode("juan.sotero@clickfactura.mx"),base64_encode($body),$pagoQueNExiste->getEmpresa());


            }


            return $this->json([
                'mensaje' => "confirmar",
            ],200);

        }

        $pago->setNumeroOperacion($datos['id']);
        $pago->setFechaOperacionBanco(\DateTime::createFromFormat('Y-m-d H:i:s',$date));
        $pago->setConcepto($datos['conceptoPago']);
        $pago->setClaveRastreo($datos['claveRastreo']);
        $pago->setCuentaOrdenante($datos['cuentaOrdenante']);
        $pago->setEstatus(2);

        $em->persist($pago);
        $em->flush();

        file_put_contents("./../respuestas/datos_recibir".$nombre.".txt",print_r($datos,true));

        $host = $pago->getSistema()->getApi();

        if(false){
            $pago->setComentario("ERROR API INTERNA NO EXISTE  ".$host);
            $em->persist($pago);
            $em->flush();

            $body = $this->renderView('cobros/error_api.html.twig',[
                'error'=>"ERROR API INTERNA NO EXISTE  ".$host
            ]);

            \App\Service\email\Email::enviarCorreo(base64_encode($pago->getEmpresa()->getMailDesde()),[$pago->getSistema()->getDesarrollador()],base64_encode("Error al consumir la api de confirmar el pago"),null,base64_encode("juan.sotero@clickfactura.mx"),base64_encode($body),$pago->getEmpresa());

        }
        else{
            try{
                $client = HttpClient::create();
                $response = $client->request('POST', $host,
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'token' => $pago->getSistema()->getToken(),
                        ],
                        //'body' => ['parameter1' => 'value1', '...'],
                        'json' => [
                            'referencia' => $pago->getReferencia(),
                            'estatus'=> "Pago exitoso",
                            'exito'=>true,
                        ],
                    ]
                );
            }catch (\Exception $ex){

            }
            $statusCode = $response->getStatusCode();

            if($statusCode != 200){

                $pago->setComentario("ERROR API INTERNA: ".print_r($response,true));

                $em->persist($pago);
                $em->flush();
            }
        }


        return $this->json([
            'mensaje' => "confirmar",
        ],200);
    }

    /**
     * @Route("/cobro/crearReferencia", name="generar_orden", methods={"POST"})
     */

    public function generarOrden(Request $request,MailerInterface $mailer):Response{

        $token = $request->headers->get('token');
        $datos = json_decode($request->getContent(),true);

        if($token == null){
            return $this->json([
                'mensaje' => "no se encontro el token",
            ],401);
        }
        else{
            $mpresaLogin = $this->getDoctrine()->getRepository(Empresas::class);
            $mpresaLoginModel = $mpresaLogin->findOneBy(['token'=>$token,'id'=>$datos['empresa']]);
        }

        if($mpresaLoginModel == null){
            return $this->json([
                'mensaje' => "token invalido",
            ],401);
        }

        if(!isset($datos['rfc'])){
            return $this->json([
                'mensaje' => "es necesario un RFC",
            ],401);
        }if(!isset($datos['mail'])){
            return $this->json([
                'mensaje' => "es necesario un correo electronico",
            ],401);
        }if(!isset($datos['monto'])){
            return $this->json([
                'mensaje' => "es necesario un monto a cobrar",
            ],401);
        }

        #busco el cliente para obtener su clabe interbancaria
        $clienteRepo = $this->getDoctrine()->getRepository(Clientes::class);
        $cliente = $clienteRepo->findOneBy(['rfc'=>$datos['rfc'],'tipo_moneda'=>$datos['moneda'],'estatus'=>1]);

        if($cliente == null){
            return $this->json([
                'mensaje' => "El cliente aun no cuenta con una cuenta para ".$datos['moneda'].", favor de registrarlo antes de generar una referencia",
            ],401);
        }

        #busco el sistema para obtener su id
        $sistemaRepo = $this->getDoctrine()->getRepository(Sistemas::class);
        $sistema = $sistemaRepo->findOneBy(['id'=>$datos['sistema'],'estatus'=>1]);

        if($sistema == null){
            return $this->json([
                'mensaje' => "El sistema no se encuentra en los registros, favor de darlo de alta",
            ],401);
        }

        #busco la empresa para obtener su id
        $empresasRepo = $this->getDoctrine()->getRepository(Empresas::class);
        $empresa = $empresasRepo->findOneBy(['id'=>$datos['empresa'],'estatus'=>1]);

        if($empresa == null){
            return $this->json([
                'mensaje' => "La empresa no se encuentra en los registros, favor de verificarlo",
            ],401);
        }

        $em=$this->getDoctrine()->getManager();

        #guardo el registro en la BD
        $modelCobro = new Pagos();

        $modelCobro->setCuentaOrdenante("");
        $modelCobro->setMonto($datos['monto']);
        $modelCobro->setFechaOperacion(\DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s')));

        $empresasRepo = $this->getDoctrine()->getRepository(Pagos::class);

        $empresaNum = $empresa->getId();
        #2 sistema.2 dia.2 empresa.2 hora.2 minuto.4 cliente. 6 monto
        $clienteNum = $cliente->getId();
        if($clienteNum <10)
            $clienteNum = "00".$clienteNum;

        if(($clienteNum < 100) && ($clienteNum > 10))
            $clienteNum = "0".$clienteNum;

        $modelPagoTotal = $empresasRepo->findBy(['empresa'=>$datos['empresa'],'cliente'=>$cliente->getId()]);

        $numTotalReferencia = count($modelPagoTotal)+1;

        #busco la referencia si ya existe, de lo contrario aumento un minuto, hasta que sea unica
        $exitePago = true;

        while ($exitePago){

            if($numTotalReferencia < 10)
                $numTotalReferencia = $numTotalReferencia."000";
            if(($numTotalReferencia < 100) && ($numTotalReferencia > 10))
                $numTotalReferencia = $numTotalReferencia."00";
            if(($numTotalReferencia < 1000) && ($numTotalReferencia > 100))
                $numTotalReferencia = $numTotalReferencia."0";

            $referenciaPagoBanco = $numTotalReferencia.$clienteNum;

            $PagoExistente = $empresasRepo->findOneBy(['empresa'=>$datos['empresa'],'referencia'=>$referenciaPagoBanco]);

            if($PagoExistente == null)
                $exitePago =  false;
            else
                $numTotalReferencia++;
        }

        $modelCobro->setReferencia($referenciaPagoBanco);
        $modelCobro->setCliente($cliente);
        $modelCobro->setEstatus(1);
        $modelCobro->setEmpresa($empresa);
        $modelCobro->setMoneda($datos['moneda']);
        //$modelCobro->setClaveRastero("");
        $modelCobro->setConcepto("");
        $modelCobro->setCuentaBeneficiario($cliente->getClabe());
        //$modelCobro->setFechaOperacionBanco(null);
        $modelCobro->setNumeroOperacion("");
        $modelCobro->setSistema($sistema);

        $em->persist($modelCobro);
        $em->flush();



        #envio el mail para tener los datos de la transferencia

        $body = $this->renderView('cobros/referencia.html.twig',[
            'referencia'=>$referenciaPagoBanco,
            'clabe'=>$cliente->getClabe(),
            'banco'=>"STP",
            'nombre'=>$cliente->getNombre(),
            'empresa_receptora'=>$empresa->getNombre(),
            'monto'=>$datos['monto'],
        ]);

        #se manda mail para confirmar registro
        $copia = null;
        if(isset($datos['copia']))
            $copia = $datos['copia'];
        \App\Service\email\Email::enviarCorreo(base64_encode("Click Factura"),[$datos['mail']],base64_encode("Referencia generada para pago"),null,$copia,base64_encode($body),$empresa);
        return $this->json([
            'mensaje' => $referenciaPagoBanco,
        ],200);
    }

    function url_exists( $url = NULL ) {

        if( empty( $url ) ){
            return false;
        }

        $ch = curl_init( $url );

        // Establecer un tiempo de espera
        curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 5 );

        // Establecer NOBODY en true para hacer una solicitud tipo HEAD
        curl_setopt( $ch, CURLOPT_NOBODY, true );
        // Permitir seguir redireccionamientos
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        // Recibir la respuesta como string, no output
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        // Descomentar si tu servidor requiere un user-agent, referrer u otra configuración específica
        // $agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36';
        // curl_setopt($ch, CURLOPT_USERAGENT, $agent)

        $data = curl_exec( $ch );

        // Obtener el código de respuesta
        $httpcode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        //cerrar conexión
        curl_close( $ch );

        // Aceptar solo respuesta 200 (Ok), 301 (redirección permanente) o 302 (redirección temporal)
        $accepted_response = array( 200, 301, 302 );
        if( in_array( $httpcode, $accepted_response ) ) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @Route("/cobro/crearCliente", name="crear_cliente", methods={"POST"})
     */
    public function crearCliente(Request $request,MailerInterface $mailer):Response{

        $token = $request->headers->get('token');
        $datos = json_decode($request->getContent(),true);

        if($token == null){
            return $this->json([
                'mensaje' => "no se encontro el token",
            ],401);
        }
        else{
            $mpresaLogin = $this->getDoctrine()->getRepository(Empresas::class);
            $mpresaLoginModel = $mpresaLogin->findOneBy(['token'=>$token,'id'=>$datos['empresa']]);

        }

        if($mpresaLoginModel == null){

            return $this->json([
                'mensaje' => "token invalido",
            ],401);

        }

        if(!isset($datos['rfc']) || strlen($datos['rfc']) == 0){
            return $this->json([
                'mensaje' => "es necesario un RFC",
            ],401);
        }

        if(!isset($datos['tipo_moneda']) || strlen($datos['tipo_moneda']) == 0){
            return $this->json([
                'mensaje' => "es necesario un tipo de moneda",
            ],401);
        }

        if(!isset($datos['cp']) || strlen($datos['cp']) == 0){
            return $this->json([
                'mensaje' => "es necesario un cp",
            ],401);
        }

        if(!isset($datos['estado']) || strlen($datos['estado']) == 0){
            return $this->json([
                'mensaje' => "es necesario un estado",
            ],401);
        }
        if(!isset($datos['empresa']) || strlen($datos['empresa']) == 0){
            return $this->json([
                'mensaje' => "es necesario un estado",
            ],401);
        }

        #busco el cliente para saber si existe o no
        #busco el cliente para obtener su clabe interbancaria
        $clienteRepo = $this->getDoctrine()->getRepository(Clientes::class);
        $cliente = $clienteRepo->findOneBy(['rfc'=>$datos['rfc'],'tipo_moneda'=>$datos['tipo_moneda'],'empresa'=>$datos['empresa'],'estatus'=>1]);


        if($cliente != null){
            return $this->json([
                'mensaje' => $cliente->getClabe(),
            ],202);
        }

        $em=$this->getDoctrine()->getManager();



        $empresaRepo = $this->getDoctrine()->getRepository(Empresas::class);
        $empresa = $empresaRepo->findOneBy(['id'=>$datos['empresa'],'estatus'=>1]);

        $cuentaCliente = $this->calcularClabe($datos['cp'],$datos['estado'],$empresa);

        /*cho $cuentaCliente;
        exit;*/

        $cliente = new Clientes();
        $cliente->setEstatus(1);
        $cliente->setEmpresa($empresa);
        $cliente->setPais($datos['pais']);
        $cliente->setMunicipio($datos['municipio']);
        $cliente->setEstado($datos['estado']);
        $cliente->setCp($datos['cp']);
        $cliente->setNombre($datos['nombre']);
        $cliente->setRfc($datos['rfc']);
        $cliente->setClabe($cuentaCliente);
        $cliente->setFechaNacimiento(\DateTime::createFromFormat('Y-m-d H:i:s',$datos['fecha_nacimiento']));
        $cliente->setNumExt($datos['num_ex']);
        $cliente->setNumInt($datos['num_int']);
        $cliente->setTipoMoneda($datos['tipo_moneda']);
        $cliente->setEmail($datos['mail']);
        $cliente->setInstitucion(646);
        $cliente->setModulo(substr($cuentaCliente,-1));
        $cliente->setPlaza(substr($cuentaCliente,3,3));
        $cliente->setPrefijo(2702);
        $cliente->setCentroCosto(substr($cuentaCliente,10,3));
        $cliente->setCuenta(substr($cuentaCliente,13,4));

        //$cliente->setTipoPersona(strlen($datos['rfc']) == 13 ? "":"");

        $em->persist($cliente);
        $em->flush();

        return $this->json([
            'mensaje' => $cuentaCliente,
        ]);

    }

    private function calcularClabe($cp,$estado,Empresas  $empresa){

        $plaza = $this->obtenerPlaza($cp,$estado, $empresa);
        $cuenta = $this->obtenerCuenta($plaza,$empresa);
        $centro_costos = $empresa->getCentroCostos();

        /*echo $plaza;
        echo "<br>";
        echo $cuenta;
        echo "<br>";
        echo $centro_costos;
        exit;*/

        $cuenta = (string)$cuenta;
        #digitos para la clabe antes de ponderar
        $digitoInstitucion1 = 6;#stp
        $digitoInstitucion2 = 4;#stp
        $digitoInstitucion3 = 6;#stp
        $digitoPlaza1 = $plaza[0];#plaza
        $digitoPlaza2 = $plaza[1];#plaza
        $digitoPlaza3 = $plaza[2];#plaza
        $digitoPrefijo1 = 2;#click
        $digitoPrefijo2 = 7;#click
        $digitoPrefijo3 = 0;#click
        $digitoPrefijo4 = 2;#click
        $digitoCuenta1 = $centro_costos[0];#centro_costos
        $digitoCuenta2 = $centro_costos[1];#centro_costos
        $digitoCuenta3 = $centro_costos[2];#centro_costos
        $digitoCuenta4 = $cuenta[0];#numero incrementable segun la plaza
        $digitoCuenta5 = $cuenta[1];#numero incrementable segun la plaza
        $digitoCuenta6 = $cuenta[2];#numero incrementable segun la plaza
        $digitoCuenta7 = $cuenta[3];#numero incrementable segun la plaza

        #digitos despues de ponderar
        $digitoInstitucionRes1 = $digitoInstitucion1 * 3;#stp
        $digitoInstitucionRes2 = $digitoInstitucion2 * 7;#stp
        $digitoInstitucionRes3 = $digitoInstitucion3 * 1;#stp
        $digitoPlazaRes1 = $digitoPlaza1 * 3;#plaza
        $digitoPlazaRes2 = $digitoPlaza2 * 7;#plaza
        $digitoPlazaRes3 = $digitoPlaza3 * 1;#plaza
        $digitoPrefijoRes1 = $digitoPrefijo1 * 3;#click
        $digitoPrefijoRes2 = $digitoPrefijo2 * 7;#click
        $digitoPrefijoRes3 = $digitoPrefijo3 * 1;#click
        $digitoPrefijoRes4 = $digitoPrefijo4 * 3;#click
        $digitoCuentaRes1 = $digitoCuenta1 * 7;#centro_costos
        $digitoCuentaRes2 = $digitoCuenta2 * 1;#centro_costos
        $digitoCuentaRes3 = $digitoCuenta3 * 3;#centro_costos
        $digitoCuentaRes4 = $digitoCuenta4 * 7;#centro_costos
        $digitoCuentaRes5 = $digitoCuenta5 * 1;#numero incrementable segun la plaza
        $digitoCuentaRes6 = $digitoCuenta6 * 3;#numero incrementable segun la plaza
        $digitoCuentaRes7 = $digitoCuenta7 * 7;#numero incrementable segun la plaza

        $digitoInstitucionMod1 = $digitoInstitucionRes1 - ( $this->truncar( ($digitoInstitucionRes1 / 10),0)) * 10;
        $digitoInstitucionMod2 = $digitoInstitucionRes2 - ( $this->truncar( ($digitoInstitucionRes2 / 10),0)) * 10;
        $digitoInstitucionMod3 = $digitoInstitucionRes3 - ( $this->truncar( ($digitoInstitucionRes3 / 10),0)) * 10;
        $digitoPlazaMod1 = $digitoPlazaRes1 - ( $this->truncar( ($digitoPlazaRes1 / 10),0)) * 10;
        $digitoPlazaMod2 = $digitoPlazaRes2 - ( $this->truncar( ($digitoPlazaRes2 / 10),0)) * 10;
        $digitoPlazaMod3 = $digitoPlazaRes3 - ( $this->truncar( ($digitoPlazaRes3 / 10),0)) * 10;
        $digitoPrefijoMod1 = $digitoPrefijoRes1 - ( $this->truncar( ($digitoPrefijoRes1 / 10),0)) * 10;
        $digitoPrefijoMod2 = $digitoPrefijoRes2 - ( $this->truncar( ($digitoPrefijoRes2 / 10),0)) * 10;
        $digitoPrefijoMod3 = $digitoPrefijoRes3 - ( $this->truncar( ($digitoPrefijoRes3 / 10),0)) * 10;
        $digitoPrefijoMod4 = $digitoPrefijoRes4 - ( $this->truncar( ($digitoPrefijoRes4 / 10),0)) * 10;
        $digitoCuentaMod1 = $digitoCuentaRes1 - ( $this->truncar( ($digitoCuentaRes1 / 10),0)) * 10;
        $digitoCuentaMod2 = $digitoCuentaRes2 - ( $this->truncar( ($digitoCuentaRes2 / 10),0)) * 10;
        $digitoCuentaMod3 = $digitoCuentaRes3 - ( $this->truncar( ($digitoCuentaRes3 / 10),0)) * 10;
        $digitoCuentaMod4 = $digitoCuentaRes4 - ( $this->truncar( ($digitoCuentaRes4 / 10),0)) * 10;
        $digitoCuentaMod5 = $digitoCuentaRes5 - ( $this->truncar( ($digitoCuentaRes5 / 10),0)) * 10;
        $digitoCuentaMod6 = $digitoCuentaRes6 - ( $this->truncar( ($digitoCuentaRes6 / 10),0)) * 10;
        $digitoCuentaMod7 = $digitoCuentaRes7 - ( $this->truncar( ($digitoCuentaRes7 / 10),0)) * 10;

        $numRes = $digitoInstitucionMod1+$digitoInstitucionMod2+$digitoInstitucionMod3+$digitoPlazaMod1+$digitoPlazaMod2+$digitoPlazaMod3+$digitoPrefijoMod1+$digitoPrefijoMod2+$digitoPrefijoMod3+$digitoPrefijoMod4+$digitoCuentaMod1+$digitoCuentaMod2+$digitoCuentaMod3+$digitoCuentaMod4+$digitoCuentaMod5+$digitoCuentaMod6+$digitoCuentaMod7;

        $numRes = $numRes- ( $this->truncar( ($numRes / 10),0)) * 10;

        if($numRes > 0)
            $numRes = 10-$numRes;

        $cuentaFinal="646".$plaza."2702".$centro_costos.$cuenta.$numRes;

        return $cuentaFinal;
    }

    private function obtenerPlaza($cp,$estado, $empresa){

        #busco el estado para asignarle su clave
        $plazaRepo = $this->getDoctrine()->getRepository(Plazas::class);
        $plaza = $plazaRepo->findOneBy(['estado'=>$estado,'estatus'=>1]);

        if($plaza != null){
            return $plaza->getNumero();
        }
        else return "";
    }

    public function obtenerCuenta($plaza,Empresas $empresa){

        $ClienteRepo = $this->getDoctrine()->getRepository(Clientes::class);
        $cliente = $ClienteRepo->findOneBy(['centro_costo'=>$empresa->getCentroCostos(),'plaza'=>$plaza,'estatus'=>1],['cuenta'=>'DESC']);

        if($cliente != null){
            return $cliente->getCuenta() + 1;
        }
        else return "1000";
    }

    function truncar($numero, $digitos)
    {
        $truncar = 10**$digitos;
        return intval($numero * $truncar) / $truncar;
    }

}