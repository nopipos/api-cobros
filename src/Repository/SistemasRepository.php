<?php

namespace App\Repository;

use App\Entity\Sistemas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sistemas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sistemas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sistemas[]    findAll()
 * @method Sistemas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SistemasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sistemas::class);
    }

    // /**
    //  * @return Sistemas[] Returns an array of Sistemas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sistemas
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
