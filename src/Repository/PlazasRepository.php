<?php

namespace App\Repository;

use App\Entity\Plazas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Plazas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Plazas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Plazas[]    findAll()
 * @method Plazas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlazasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Plazas::class);
    }

    // /**
    //  * @return Plazas[] Returns an array of Plazas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Plazas
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
