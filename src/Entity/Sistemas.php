<?php

namespace App\Entity;

use App\Repository\SistemasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SistemasRepository::class)
 */
class Sistemas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $api;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=Pagos::class, mappedBy="sistema")
     */
    private $sistema;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $desarrollador;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empresa")
     */
    private $empresa;

    public function __construct()
    {
        $this->sistema = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApi(): ?string
    {
        return $this->api;
    }

    public function setApi(string $api): self
    {
        $this->api = $api;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|Pagos[]
     */
    public function getSistema(): Collection
    {
        return $this->sistema;
    }

    public function addSistema(Pagos $sistema): self
    {
        if (!$this->sistema->contains($sistema)) {
            $this->sistema[] = $sistema;
            $sistema->setSistema($this);
        }

        return $this;
    }

    public function removeSistema(Pagos $sistema): self
    {
        if ($this->sistema->removeElement($sistema)) {
            // set the owning side to null (unless already changed)
            if ($sistema->getSistema() === $this) {
                $sistema->setSistema(null);
            }
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getDesarrollador(): ?string
    {
        return $this->desarrollador;
    }

    public function setDesarrollador(string $desarrollador): self
    {
        $this->desarrollador = $desarrollador;

        return $this;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }
}
