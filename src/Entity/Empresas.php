<?php

namespace App\Entity;

use App\Repository\EmpresasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpresasRepository::class)
 */
class Empresas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $rfc;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=Clientes::class, mappedBy="empresa")
     */
    private $clientes;

    /**
     * @ORM\OneToMany(targetEntity=Pagos::class, mappedBy="empresa")
     */
    private $empresas;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $centro_costos;

    /**
     * @ORM\OneToMany(targetEntity=Sistemas::class, mappedBy="empresa")
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=140)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=140)
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $mail_desde;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $puerto;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $host;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    public function __construct()
    {
        $this->clientes = new ArrayCollection();
        $this->empresas = new ArrayCollection();
        $this->empresa = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|Clientes[]
     */
    public function getClientes(): Collection
    {
        return $this->clientes;
    }

    public function addCliente(Clientes $cliente): self
    {
        if (!$this->clientes->contains($cliente)) {
            $this->clientes[] = $cliente;
            $cliente->setEmpresa($this);
        }

        return $this;
    }

    public function removeCliente(Clientes $cliente): self
    {
        if ($this->clientes->removeElement($cliente)) {
            // set the owning side to null (unless already changed)
            if ($cliente->getEmpresa() === $this) {
                $cliente->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Pagos[]
     */
    public function getEmpresas(): Collection
    {
        return $this->empresas;
    }

    public function addEmpresa(Pagos $empresa): self
    {
        if (!$this->empresas->contains($empresa)) {
            $this->empresas[] = $empresa;
            $empresa->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpresa(Pagos $empresa): self
    {
        if ($this->empresas->removeElement($empresa)) {
            // set the owning side to null (unless already changed)
            if ($empresa->getEmpresa() === $this) {
                $empresa->setEmpresa(null);
            }
        }

        return $this;
    }

    public function getCentroCostos(): ?string
    {
        return $this->centro_costos;
    }

    public function setCentroCostos(string $centro_costos): self
    {
        $this->centro_costos = $centro_costos;

        return $this;
    }

    /**
     * @return Collection|Sistemas[]
     */
    public function getEmpresa(): Collection
    {
        return $this->empresa;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getMailDesde(): ?string
    {
        return $this->mail_desde;
    }

    public function setMailDesde(string $mail_desde): self
    {
        $this->mail_desde = $mail_desde;

        return $this;
    }

    public function getPuerto(): ?string
    {
        return $this->puerto;
    }

    public function setPuerto(string $puerto): self
    {
        $this->puerto = $puerto;

        return $this;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(string $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
