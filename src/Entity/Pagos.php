<?php

namespace App\Entity;

use App\Repository\PagosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PagosRepository::class)
 */
class Pagos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Sistemas::class, inversedBy="sistema")
     */
    private $sistema;

    /**
     * @ORM\ManyToOne(targetEntity=Clientes::class, inversedBy="clientes")
     */
    private $cliente;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empresas")
     */
    private $empresa;

    /**
     * @ORM\Column(type="float")
     */
    private $monto;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $moneda;

    /**
     * @ORM\Column(type="string", length=130)
     */
    private $concepto;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $referencia;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $numero_operacion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha_operacion;

    /**
     * @ORM\Column(type="datetime" , nullable=true)
     */
    private $fecha_operacion_banco;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $clave_rastreo;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $cuenta_ordenante;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $cuenta_beneficiario;

    /**
     * @ORM\Column(type="integer", options={"comment":"1 registro nuevo, 2 registro exitoso con respuesta del banco, 3 registro devuelto, 4 registro no identificado"})
     */
    private $estatus;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comentario;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSistema(): ?Sistemas
    {
        return $this->sistema;
    }

    public function setSistema(?Sistemas $sistema): self
    {
        $this->sistema = $sistema;

        return $this;
    }

    public function getCliente(): ?Clientes
    {
        return $this->cliente;
    }

    public function setCliente(?Clientes $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getMonto(): ?string
    {
        return $this->monto;
    }

    public function setMonto(string $monto): self
    {
        $this->monto = $monto;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    public function setConcepto(string $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }

    public function getReferencia(): ?string
    {
        return $this->referencia;
    }

    public function setReferencia(string $referencia): self
    {
        $this->referencia = $referencia;

        return $this;
    }

    public function getNumeroOperacion(): ?string
    {
        return $this->numero_operacion;
    }

    public function setNumeroOperacion(string $numero_operacion): self
    {
        $this->numero_operacion = $numero_operacion;

        return $this;
    }

    public function getFechaOperacion(): ?\DateTimeInterface
    {
        return $this->fecha_operacion;
    }

    public function setFechaOperacion(\DateTimeInterface $fecha_operacion): self
    {
        $this->fecha_operacion = $fecha_operacion;

        return $this;
    }

    public function getFechaOperacionBanco(): ?\DateTimeInterface
    {
        return $this->fecha_operacion_banco;
    }

    public function setFechaOperacionBanco(\DateTimeInterface $fecha_operacion_banco): self
    {
        $this->fecha_operacion_banco = $fecha_operacion_banco;

        return $this;
    }

    public function getClaveRastero(): ?string
    {
        return $this->clave_rastreo;
    }

    public function setClaveRastreo(?string $clave_rastreo): self
    {
        $this->clave_rastreo = $clave_rastreo;

        return $this;
    }

    public function getCuentaOrdenante(): ?string
    {
        return $this->cuenta_ordenante;
    }

    public function setCuentaOrdenante(string $cuenta_ordenante): self
    {
        $this->cuenta_ordenante = $cuenta_ordenante;

        return $this;
    }

    public function getCuentaBeneficiario(): ?string
    {
        return $this->cuenta_beneficiario;
    }

    public function setCuentaBeneficiario(string $cuenta_beneficiario): self
    {
        $this->cuenta_beneficiario = $cuenta_beneficiario;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getComentario(): ?string
    {
        return $this->comentario;
    }

    public function setComentario(?string $comentario): self
    {
        $this->comentario = $comentario;

        return $this;
    }
}
