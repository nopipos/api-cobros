<?php

namespace App\Entity;

use App\Repository\ClientesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientesRepository::class)
 */
class Clientes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="clientes")
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $rfc;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $pais;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $estado;

    /**
     * @ORM\Column(type="string", length=55)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=110, nullable=true)
     */
    private $municipio;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $num_ext;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $num_int;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_nacimiento;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $tipo_persona;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $clabe;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=Pagos::class, mappedBy="cliente")
     */
    private $clientes;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $tipo_moneda;

    /**
     * @ORM\Column(type="integer")
     */
    private $institucion;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $plaza;

    /**
     * @ORM\Column(type="integer")
     */
    private $prefijo;

    /**
     * @ORM\Column(type="integer")
     */
    private $cuenta;

    /**
     * @ORM\Column(type="integer")
     */
    private $modulo;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $centro_costo;

    public function __construct()
    {
        $this->clientes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->pais;
    }

    public function setPais(string $pais): self
    {
        $this->pais = $pais;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    public function setMunicipio(?string $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getNumExt(): ?string
    {
        return $this->num_ext;
    }

    public function setNumExt(?string $num_ext): self
    {
        $this->num_ext = $num_ext;

        return $this;
    }

    public function getNumInt(): ?string
    {
        return $this->num_int;
    }

    public function setNumInt(?string $num_int): self
    {
        $this->num_int = $num_int;

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fecha_nacimiento;
    }

    public function setFechaNacimiento(?\DateTimeInterface $fecha_nacimiento): self
    {
        $this->fecha_nacimiento = $fecha_nacimiento;

        return $this;
    }

    public function getTipoPersona(): ?string
    {
        return $this->tipo_persona;
    }

    public function setTipoPersona(string $tipo_persona): self
    {
        $this->tipo_persona = $tipo_persona;

        return $this;
    }

    public function getClabe(): ?string
    {
        return $this->clabe;
    }

    public function setClabe(string $clabe): self
    {
        $this->clabe = $clabe;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|Pagos[]
     */
    public function getClientes(): Collection
    {
        return $this->clientes;
    }

    public function addCliente(Pagos $cliente): self
    {
        if (!$this->clientes->contains($cliente)) {
            $this->clientes[] = $cliente;
            $cliente->setCliente($this);
        }

        return $this;
    }

    public function removeCliente(Pagos $cliente): self
    {
        if ($this->clientes->removeElement($cliente)) {
            // set the owning side to null (unless already changed)
            if ($cliente->getCliente() === $this) {
                $cliente->setCliente(null);
            }
        }

        return $this;
    }

    public function getTipoMoneda(): ?string
    {
        return $this->tipo_moneda;
    }

    public function setTipoMoneda(string $tipo_moneda): self
    {
        $this->tipo_moneda = $tipo_moneda;

        return $this;
    }

    public function getInstitucion(): ?int
    {
        return $this->institucion;
    }

    public function setInstitucion(int $institucion): self
    {
        $this->institucion = $institucion;

        return $this;
    }

    public function getPlaza(): ?string
    {
        return $this->plaza;
    }

    public function setPlaza(string $plaza): self
    {
        $this->plaza = $plaza;

        return $this;
    }

    public function getPrefijo(): ?int
    {
        return $this->prefijo;
    }

    public function setPrefijo(int $prefijo): self
    {
        $this->prefijo = $prefijo;

        return $this;
    }

    public function getCuenta(): ?int
    {
        return $this->cuenta;
    }

    public function setCuenta(int $cuenta): self
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    public function getModulo(): ?int
    {
        return $this->modulo;
    }

    public function setModulo(int $modulo): self
    {
        $this->modulo = $modulo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCentroCosto(): ?string
    {
        return $this->centro_costo;
    }

    public function setCentroCosto(string $centro_costo): self
    {
        $this->centro_costo = $centro_costo;

        return $this;
    }
}
